# @semantic-release/config-gitlab-npm

> [**semantic-release**](https://github.com/semantic-release/semantic-release)
> config to publish a [NPM](https://www.npmjs.com/) package.

## Getting Started

Add the following to `.releaserc.yaml`:

```yaml
extends:
  - "@semantic-release/config-gitlab-npm"
```

Combine that with semantic-release/config-release-channels>:

```yaml
extends:
  - "@semantic-release/config-release-channels"
  - "@semantic-release/config-gitlab-npm"
```

Add the following to your Semantic Release job in `.gitlab-ci.yml`:

```yaml
...

semantic-release:
  ...
  before_script:
    - npm config set -- "@${CI_PROJECT_ROOT_NAMESPACE}:registry"
      "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/npm/"
    - npm config set -- "${CI_API_V4_URL#https?}/packages/npm/:_authToken"
      '\${CI_JOB_TOKEN}'
  ...

...
```

## Configuration

### Authentication

#### GitLab

Create a project access token with `api`/`write_repository` permissions.

Add the token as a `GITLAB_TOKEN` CI protected/masked variable.
